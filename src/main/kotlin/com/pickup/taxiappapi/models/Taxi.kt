package com.pickup.taxiappapi.models

import com.pickup.taxiappapi.security.User
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class Taxi(
        @Id
        var id: UUID = UUID.randomUUID(),

        @OneToOne
        val driver: User,

        @OneToOne
        val location: Location,

        @OneToOne
        val vehicle: Vehicle
)
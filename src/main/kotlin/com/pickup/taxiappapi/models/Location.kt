package com.pickup.taxiappapi.models

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Location(
        @Id
        var id: UUID = UUID.randomUUID(),

        val latitude: Double,

        val longitude: Double
)
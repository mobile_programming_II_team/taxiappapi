package com.pickup.taxiappapi.models

import com.pickup.taxiappapi.security.User
import java.util.*
import javax.persistence.*

@Entity
data class Fare(
        @Id
        var id: UUID = UUID.randomUUID(),

        @OneToOne
        val taxi: Taxi,

        @OneToOne
        val passenger: User,

        @OneToOne
        val startPoint: Location,

        @OneToOne
        val destination: Location,

        @OneToOne
        val feedback: Feedback,

        @Enumerated(EnumType.STRING)
        val status: Status) {

    enum class Status {
        REQUESTED,
        ACCEPTED,
        STARTED,
        COMPLETED,
        CANCELED
    }
}
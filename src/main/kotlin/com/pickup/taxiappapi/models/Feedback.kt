package com.pickup.taxiappapi.models

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Lob

@Entity
class Feedback(
        @Id
        var id: UUID = UUID.randomUUID(),

        val rating: Int,

        @Lob
        val review: String)
package com.pickup.taxiappapi.models

import java.util.*
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

@Entity
data class Vehicle(
        @Id
        var id: UUID = UUID.randomUUID(),

        val vehicleType: String,

        @Enumerated(EnumType.STRING)
        val grade: Grade) {

    enum class Grade {
        HIGH_END,
        AVERAGE,
        LOW_END
    }
}
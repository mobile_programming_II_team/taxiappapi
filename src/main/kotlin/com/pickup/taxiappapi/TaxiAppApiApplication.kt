package com.pickup.taxiappapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TaxiAppApiApplication

fun main(args: Array<String>) {
    runApplication<TaxiAppApiApplication>(*args)
}

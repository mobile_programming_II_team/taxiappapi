package com.pickup.taxiappapi.security

import com.pickup.taxiappapi.models.Feedback
import com.pickup.taxiappapi.models.Location
import java.util.*
import javax.persistence.*

@Entity
data class User(
        @Id
        var id: UUID = UUID.randomUUID(),

        val name: String,

        val email: String,

        val password: String,

        @OneToOne
        val location: Location,

        @OneToOne
        val feedback: Feedback,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinTable(name = "user_role", joinColumns = [JoinColumn(name = "user_id")], inverseJoinColumns = [JoinColumn(name = "role_id")])
        var roles: Set<Role>? = mutableSetOf()
)
package com.pickup.taxiappapi.security

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Role(
        @Id
        var id: UUID = UUID.randomUUID(),

        val role: String
)
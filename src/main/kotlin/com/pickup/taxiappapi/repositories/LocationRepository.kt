package com.pickup.taxiappapi.repositories

import com.pickup.taxiappapi.models.Location
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface LocationRepository : JpaRepository<Location, UUID> {
    fun findByLatitudeAndLongitude(latitude: Double, longitude: Double): Optional<Location>
}
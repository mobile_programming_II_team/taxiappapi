# Create Databases
CREATE DATABASE if not exists taxi_database;

# Create user
CREATE USER if not exists 'taxi_user'@'localhost' IDENTIFIED BY 'taxi_password';

# Database Grants
GRANT ALL ON taxi_database.* to 'taxi_user'@'localhost';